<?php
/*
Plugin Name: Move Uploads Folder
Version: 0.1.1
*/

/**
 * Move the "uploads" folder
 */
function move_uploads_dir( $upload_dir ) {
	if ( defined('WEBROOT_DIR') && defined('WP_HOME') ) {
    $upload_dir['path'] = WEBROOT_DIR . '/media' . $upload_dir['subdir'];
    $upload_dir['url'] = WP_HOME . '/media' . $upload_dir['subdir'];
    $upload_dir['basedir'] = WEBROOT_DIR . '/media';
    $upload_dir['baseurl'] = WP_HOME . '/media';
  }

	return $upload_dir;
}
add_filter( 'upload_dir', 'move_uploads_dir', 1, 1);
